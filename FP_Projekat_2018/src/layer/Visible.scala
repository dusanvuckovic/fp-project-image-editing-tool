package layer

trait Visible {
  var visible = true
  
  def setVisible = 
    visible = true
  def setInvisible = 
    visible = false
  def flipVisibility = 
    visible = !visible
    
  def writeVisible: String =
    if (visible) "V" else "IV"
      
  def toggleVisible(visible: Boolean) =
    this.visible = visible
}