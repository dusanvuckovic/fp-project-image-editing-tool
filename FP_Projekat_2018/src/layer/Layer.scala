package layer

import image.Image
import selection.Active
import selection.Named
import image.Position
import image.ImageUtility
import selection.Area
import image.Pixel

object Layer {   
  
  def fromMF(s: String, originalImage: Image): Layer = {
    //s"layer ${image.fpath} $active $visible ${translate.x} ${translate.y}"
    val data = s.split(" ")
    val fpath = data(0)
    val opacity = data(1).toDouble
    val active = data(2).toBoolean
    val visible = data(3).toBoolean
    val tX = data(4).toInt
    val tY = data(5).toInt
    
    val image = ImageUtility.load(fpath)
    val layer = new Layer(image, originalImage, opacity)
    layer.toggleActive(active)
    layer.toggleVisible(visible)
    layer.translate = Position(tX, tY)
    layer    
  }  
}

class Layer(val image: Image, val originalImage: Image, 
    var _opacity: Double) extends Area(image.width, image.height) with Active with Visible {
  
  var translate = Position(0, 0)
  
  def this(image: Image, originalImage: Image) = this(image, originalImage, 1d)
  
  private def getTranslatedBottom: String = 
    (translate + Position(image.width, image.height)).toString         
   
  def translateTo(position: Position) = {
    val xOkay = (position.x + image.width) <= originalImage.width
    val yOkay = (position.y + image.height) <= originalImage.height
    if (!(xOkay && yOkay))
      throw new TranslationOutsideBoundsException("Dimenzije slike nisu odgovarajuće!")
    translate = position    
  }
     
  override def rangeX: Range = 
    Range(left + translate.x, right + translate.x)
    
  override def rangeY: Range = 
    Range(top + translate.y, bottom + translate.y)
    
  def apply(i: Int, j: Int): Pixel = {
    image(i-translate.x, j-translate.y)
  }  
  
  def show: Unit = 
    ImageUtility.show(image, _opacity)
  
  def opacity: Double = _opacity
  
  def opacity_= (opacity: Double): Unit = {
    if (opacity < 0 || opacity > 1d)
      throw new OpacityOutsideBoundsException
    _opacity = opacity   
  }
  
  def translateTo(x: Int, y: Int): Unit = translateTo(Position(x, y))
  
  def translateToTopLeft = translateTo(0, 0)
  
  def translateToBottomLeft = translateTo(0, originalImage.height - image.height)
    
  def translateToTopRight = translateTo(originalImage.width - image.width, 0)   
  
  def translateToBottomRight = 
    translateTo(originalImage.width - image.width, originalImage.height - image.height)
        
  override def toString: String = {
    val active = this.writeActive
    val visible = this.writeVisible    
    f"[$active%s][$visible%s][$opacity%.2f] from $translate%s to $getTranslatedBottom%s"    
  }
  
  def toMF = {
    s"layer ${image.fpath} $opacity $active $visible ${translate.x} ${translate.y}"    
  }
    
} 