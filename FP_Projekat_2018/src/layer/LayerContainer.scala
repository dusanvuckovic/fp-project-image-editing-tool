package layer

import scala.collection.mutable.ArrayBuffer
import scala.language.postfixOps
import image.Position

class LayerContainer extends Iterable[Layer] {
  
  val layers: ArrayBuffer[Layer] = new ArrayBuffer(5)
  
  def +=(l: Layer) = {
    layers += l
  }
  
  def clear = layers.clear()
  
  
  def -=(id: Int) = {
    val l = this(id)
    layers -= l       
  }
  
  override def iterator = 
    layers.iterator 
    
  def apply(i: Int): Layer = { /*zapravo () */
    val index = i-1
    if (index < 0 || index >= layers.size)
      throw new IndexOutOfBoundsException("Odabrali ste nepostojeći sloj!")
    layers(index)       
  }
  
  def update(i: Int, l: Layer) = {
    val index = i-1
    layers(index) = l    
  }
  
  def activate(id: Int) =
    this(id).setActive  
  
  def deactivate(id: Int) = 
    this(id).setInactive    
  
  /* Napomena: slojevi su po postavci poređani od 1 do N, 
   * gde je N na vrhu, a slika je "(N+1)-vi" sloj. Kad kažemo
   * "pomeri gore", mislimo "pomeri bliže slici" (dakle, ++ u ovom
   * formatu).
   * */     
    
  def move(id: Int, pos: Int) {
    val current = this(id)
    this(id) = this(pos)
    this(pos) = current    
  }  
    
  def moveUp(id: Int) = {    
    val current = this(id)
    this(id) = this(id+1)
    this(id+1) = current    
  }
  
  def moveDown(id: Int) = {
    val current = this(id-1)
    this(id-1) = this(id)
    this(id) = current    
  }
  
  def translateTo(id: Int, position: Position) = {
    this(id).translateTo(position)
    
  }
  def translateToTopLeft(id: Int) = {
    this(id).translateToTopLeft
  }
  def translateToBottomLeft(id: Int) = {
    this(id).translateToBottomLeft
  }
  def translateToTopRight(id: Int) = {
    this(id).translateToTopRight
  }
  def translateToBottomRight(id: Int) = {
    this(id).translateToBottomRight
  }
  
  override def toString: String = {
    val sb = new StringBuilder("Slojevi:")    
    for ((description, index) <- layers map (_.toString) zipWithIndex) {
      val id = index + 1
      sb.append(s"\n\tSloj #$id: $description")
    }            
    sb.toString
  }
  
  
}