package operation.predefined

import operation.CompositeFunction
import operation.arithmetic.SubOperation
import operation.functional.MaxFunction
import operation.Function
import operation.arithmetic.SubInvOperation
import operation.UnaryOperation

object InversionFunction extends UnaryOperation {
  override def op(color: Double): Double = SubInvOperation.op(color)(1d)      
}