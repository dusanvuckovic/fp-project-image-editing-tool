package operation.predefined

import operation.Function
import image.Pixel

object GrayscaleFunction extends Function {
  
  override def f(p: Pixel): Pixel = {
    val avg = (p.color.r + p.color.g + p.color.b)/3d
    p(_ => avg)    
  } 
    
}