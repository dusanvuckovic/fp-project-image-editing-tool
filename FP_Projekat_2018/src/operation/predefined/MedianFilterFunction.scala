package operation.predefined

import image.Image
import image.Pixel
import image.Position
import selection.Area
import image.Color
import scala.collection.mutable.ArrayBuffer

class MedianFilterFunction(n: Int, im: Image) extends FilterFunction(n: Int, im: Image) {
   
  override def filter(centerPixel: Pixel): Pixel = {
    val area = this.area(centerPixel)
    
    val b = new ArrayBuffer[Pixel]
    val x = centerPixel.position.x
    val y = centerPixel.position.y
    
    /*val pixels = im.filter(p => area.isIn(p)).sorted*/
    /* korišćenje filter operacije nad nizom svih piksela slike
     * je užasno sporo, jer se za svaki piksel proverava prisustvo oblasti
     * area. Ima W*L piksela, to proveravamo N puta za svaki centralni
     * piksel u selekciji (P*Q), pa je to O(n^5), u poređenju sa iterativnim rešenjem
     * */
    for {
      i <- area.rangeX
      j <- area.rangeY                
    } b += im(i, j)
    
    val pixels = b.sorted             
    val len = pixels.length
    val color = 
      if (len % 2 == 0) 
        pixels(len/2).color
      else 
        (pixels(len/2).color + pixels(len/2+1).color) / 2
    centerPixel.color = color
    centerPixel
  }
  
}