package operation.predefined

import image.Image
import image.Pixel
import image.Color

class WeightedMeanFilterFunction(n: Int, matrix: Array[Array[Double]], im: Image) extends FilterFunction(n: Int, im: Image) {
  
    override def filter(centerPixel: Pixel): Pixel = {
      val area = this.area(centerPixel)
      val x = centerPixel.position.x
      val y = centerPixel.position.y
      var color: Color = new Color(0, 0, 0)      
            
      for {
        i <- area.rangeX
        j <- area.rangeY                
      } color += im(i, j).color * matrix(Math.abs(i-x))(Math.abs(j-y))
      
      val ponderVal = matrix.flatten.reduce((x, y) => x + y)      
      
      centerPixel.color = color / ponderVal
      centerPixel
  }
  
}