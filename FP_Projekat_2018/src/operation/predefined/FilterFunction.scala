package operation.predefined

import operation.Function
import image.Pixel
import image.Image
import image.Position
import selection.Area

abstract class FilterFunction(n: Int, im: Image) extends Function {
     
  def filter(centerPixel: Pixel): Pixel
  
  protected def area(centerPixel: Pixel) = {
    val limit = Math.min(im.height, im.width)
    
    val posSt = Position(
        Math.max(0, centerPixel.position.x-n), 
        Math.max(0, centerPixel.position.y-n)
    )
    val posEd = Position(
        Math.min(limit, centerPixel.position.x+n), 
        Math.min(limit, centerPixel.position.y+n)
    ) 
    
    new Area(posSt, posEd)
    
  } 
      
  override def f(p: Pixel): Pixel = {
    filter(p) 
    //p
  } 
  
  
}