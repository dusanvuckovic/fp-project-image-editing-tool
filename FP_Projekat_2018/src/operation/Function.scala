package operation

import image.Pixel
import selection.Named

trait Function {  
  private[operation] def f(p: Pixel): Pixel
  
  def apply(p: Pixel): Pixel 
    = f(p)          
    
}