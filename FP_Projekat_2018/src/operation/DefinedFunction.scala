package operation

import image.Pixel

class DefinedFunction(val func: (Pixel => Pixel)) extends Function {
  override def f(p: Pixel): Pixel 
    = func(p)
}