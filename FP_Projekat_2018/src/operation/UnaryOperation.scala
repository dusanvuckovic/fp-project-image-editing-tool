package operation

import image.Pixel

trait UnaryOperation extends Operation {
  
  def op(color: Double): Double
  
  override def f(pixel: Pixel): Pixel = {
    pixel.color.r = op(pixel.color.r)
    pixel.color.g = op(pixel.color.g)
    pixel.color.b = op(pixel.color.b)
    pixel
  }    
}