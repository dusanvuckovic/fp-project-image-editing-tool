package operation.functional

import operation.Function
import image.Pixel
import operation.BinaryOperation
import operation.arithmetic.ArithmeticOperation

object LogFunction extends Function {
  
  override def f(p: Pixel): Pixel = {
    p(x => Math.log(x))  
  }
  
}