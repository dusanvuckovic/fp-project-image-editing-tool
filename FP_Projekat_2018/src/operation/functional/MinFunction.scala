package operation.functional

import operation.Function
import image.Pixel
import operation.arithmetic.ArithmeticOperation

object MinFunction extends ArithmeticOperation {
  
  override def op(color: Double)(parameter: Double): Double =
    Math.min(color, parameter)
  
}