package operation.functional

import operation.Function
import image.Pixel
import operation.arithmetic.ArithmeticOperation

object MaxFunction extends ArithmeticOperation {
  
 override def op(color: Double)(parameter: Double): Double =
    Math.max(color, parameter)
  
}