package operation.functional

import operation.Function
import image.Pixel
import operation.arithmetic.ArithmeticOperation

object PowFunction extends ArithmeticOperation {
  
  override def op(color: Double)(parameter: Double): Double =
    Math.pow(color, parameter)       
    
}