package operation.functional

import operation.Function
import image.Pixel

object AbsFunction extends Function {
  
  override def f(p: Pixel): Pixel = {
    p(x => Math.abs(x))  
  }  
}