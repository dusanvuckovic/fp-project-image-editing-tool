package operation

import image.Pixel
import javax.naming.OperationNotSupportedException

class FillOperation(r: Double, g: Double, b: Double) extends UnaryOperation {
  override def op(color: Double): Double = {
    throw new OperationNotSupportedException("Selekcija jedne komponente nema smisla pri punjenju!")
  }
  
  override def f(pixel: Pixel): Pixel = {
    pixel.color.r = r
    pixel.color.g = g
    pixel.color.b = b
    pixel
  }   
}