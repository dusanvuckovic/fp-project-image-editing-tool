package operation

import image.Pixel

class SequentialOperation (functions: List[Function], val ops: String) extends MultipartFunction {
  override def f(p: Pixel): Pixel = {        
      
    val sequentialF = functions.reduceLeft((a, b) => {
      val fp1 = a.f(_)
      val fp2 = b.f(_)
      new DefinedFunction(fp1 andThen fp2)      
    })   
    sequentialF(p)   
		
  }
  
  override def toMF = {
    s"sequential $ops"      
  }
  
}