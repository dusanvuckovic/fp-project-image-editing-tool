package operation

import image.Pixel

class CompositeFunction(functions: List[Function], val ops: String) extends MultipartFunction {
  
    override def f(p: Pixel): Pixel = {
     
      val composedF = functions.reduceLeft((a, b) => {
        val fp1 = a.f(_)
        val fp2 = b.f(_)
        new DefinedFunction(fp1 compose fp2)      
      })   
      
      composedF(p)
    }
    
    override def toMF = {
      s"composite $ops"      
    }
    
}