package operation

import image.Pixel

trait BinaryOperation[T] {
  
  def op(color: Double)(parameter: T): Double
  
  def f(pixel: Pixel, parameter: T): Pixel = {
    pixel.color.r = op(pixel.color.r)(parameter)
    pixel.color.g = op(pixel.color.g)(parameter)
    pixel.color.b = op(pixel.color.b)(parameter)
    pixel
  }        
  
  def toFunction(parameter: T): DefinedFunction = {
    val fn = f(_: Pixel, parameter)
    new DefinedFunction(fn)
  }
  
}