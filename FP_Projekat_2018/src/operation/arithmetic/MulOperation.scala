package operation.arithmetic

object MulOperation extends ArithmeticOperation {
  
  override def op(color: Double)(parameter: Double): Double =
    color * parameter
}