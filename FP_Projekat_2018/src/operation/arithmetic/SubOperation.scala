package operation.arithmetic

object SubOperation extends ArithmeticOperation {
  
  override def op(color: Double)(parameter: Double): Double =
      color - parameter
}