package operation.arithmetic

object AddOperation extends ArithmeticOperation {  
  override def op(color: Double)(parameter: Double): Double = {
    color + parameter
  } 
}