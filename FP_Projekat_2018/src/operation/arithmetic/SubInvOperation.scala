package operation.arithmetic

object SubInvOperation extends ArithmeticOperation {  
  override def op(color: Double)(parameter: Double): Double =
      parameter - color
}