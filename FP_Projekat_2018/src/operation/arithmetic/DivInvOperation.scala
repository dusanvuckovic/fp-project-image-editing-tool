package operation.arithmetic

object DivInvOperation extends ArithmeticOperation {
  
  override def op(color: Double)(parameter: Double): Double =
    parameter / color
  
}