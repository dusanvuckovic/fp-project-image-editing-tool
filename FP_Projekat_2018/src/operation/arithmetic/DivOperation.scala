package operation.arithmetic

object DivOperation extends ArithmeticOperation {
  
    override def op(color: Double)(parameter: Double): Double =
      color / parameter
}