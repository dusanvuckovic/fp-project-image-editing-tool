package operation.arithmetic

import operation.BinaryOperation

trait ArithmeticOperation extends BinaryOperation[Double] {
  
}