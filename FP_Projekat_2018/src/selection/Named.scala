package selection

trait Named {
  val name: String
}