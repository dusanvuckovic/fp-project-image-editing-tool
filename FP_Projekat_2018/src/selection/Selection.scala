package selection

import scala.collection.mutable.ArrayBuffer
import image.Position
import image.Pixel

object Selection {
  def fromMF(s: String): Selection = {
    var data = s.split(" ")    
    val name = data(0)
    val active = data(1).toBoolean
    data = data.drop(2)
    val sel = new Selection(name)    
    while (data.nonEmpty) {
      val partData = data.take(4)
      data = data.drop(4)      
      val p1x = partData(0).toInt
      val p1y = partData(1).toInt
      val p2x = partData(2).toInt
      val p2y = partData(3).toInt  
      sel += (Position(p1x, p1y), Position(p2x, p2y))
    }
           
    sel.toggleActive(active)
    sel
  }    
}

class Selection(val name: String) extends Named with Active {
     
  private var currentID = 0
  def nextID: Int = {
    currentID += 1
    currentID    
  }   
  
  def prevID: Unit = currentID -= 1  
    
  val parts: ArrayBuffer[SelectionPart] = ArrayBuffer.empty
  
  def this(name: String, p1: Position, p2: Position) = {
    this(name)
    parts += SelectionPart(p1, p2, nextID)
  }
  
  def +=(p1: Position, p2: Position): Unit = {    
    val testPart = SelectionPart(p1, p2, nextID)
    val intersects = parts.filter(p => p.intersects(testPart))
    if (intersects.nonEmpty) {
      prevID
      throw new Exception("Podudarnost sa selekcijama: " + intersects.mkString("\n"))      
    }
    else {
      parts += testPart      
    }           
  }
  
  def containsPixel(p: Pixel): Boolean = 
    if (parts.isEmpty) 
      true
    else  
      parts.exists(_.contains(p.position))
      
  def -=(part: SelectionPart): Unit =
    this.parts -= part
    
    
  /* Neefikasno, ali prikazuje više mogućnosti Skale od var parts sa filtriranjem ili 
   * iterativne pretrage za izbacivanje 
   */   
        
  def -=(id: Int): Unit = {    
    val badParts = parts.filter(_.id == id)
    if (badParts.isEmpty)
      throw new Exception(s"Selekcija $name ne sadrži deo pod brojem $id!")
    parts --= badParts
  }      
    
  override def toString: String = {
    var s = s"Selekcija $name[${writeActive}]: "
    if (parts.size == 0)
      s += "\n\t[prazna]"
    else
      parts.foreach(part => s += part)      
    s    
  }
  
  def toMF = {    
    var str = s"selection $name $active "
    parts.map(part => str += part.toMF + " ")
    str.trim
    
  }
 
}