package selection

import image.Position

case class SelectionPart(val topLeft: Position, val bottomRight: Position, val id: Int) 
  extends Rectangular {
  
  override def toString: String =
    s"\n\tdeo #$id: " + getRange
    
  def toMF = {
    s"${topLeft.x} ${topLeft.y} ${bottomRight.x} ${bottomRight.y}"    
  }
  
}