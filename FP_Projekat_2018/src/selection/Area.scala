package selection

import image.Position

class Area(val topLeft: Position, val bottomRight: Position) extends Rectangular {
  
  def this(width: Int, height: Int) 
    = this(Position(0, 0), Position(width, height))
  
  def intersection(that: Rectangular): Area = {
    if (this intersects that)
      throw new Exception("Nema preseka!")
    val tLeft = new Position(
        Math.max(topLeft.x, that.topLeft.x), Math.max(topLeft.y, that.topLeft.y)
    )
    val bRight = new Position(
        Math.min(bottomRight.x, that.bottomRight.x), Math.min(bottomRight.y, that.bottomRight.y)
    )    
    new Area(tLeft, bRight)
  }
  
  def rangeX =
    Range(left, right)
  
  
  def rangeY =
    Range(top, bottom)
  
}