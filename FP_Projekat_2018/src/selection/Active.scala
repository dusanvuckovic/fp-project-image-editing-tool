package selection

trait Active {
  var active: Boolean = true
  
  def setActive = 
    active = true
  def setInactive =
    active = false
  def flipActivity =
    active = !active
    
  def writeActive: String =
    if (active) "A" else "IA"      
  
  def toggleActive(active: Boolean) = 
    this.active = active
}