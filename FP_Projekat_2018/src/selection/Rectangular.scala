package selection

import image.Position

trait Rectangular {
  val topLeft: Position
  val bottomRight: Position
  
  def left = topLeft.x
  def right = bottomRight.x
  def top = topLeft.y
  def bottom = bottomRight.y
  
  def width = bottomRight.x
  def height = bottomRight.y
  
  def contains(pos: Position): Boolean = {
    val inX = pos.x >= left && pos.x < right
    val inY = pos.y >= top && pos.y < bottom
    inX && inY    
  }
  
  def getRange: String = 
    s"from $topLeft to $bottomRight"         
    
  def intersects(rectB: Rectangular): Boolean = {
    val rectA: Rectangular = this
    val isLeft = rectA.left > rectB.right
    val isRight = rectA.right < rectB.left
    val isAbove = rectA.bottom < rectB.top /*obrnuto zbog y usmerenog nadole*/
    val isBelow = rectA.top > rectB.bottom
    
    !(isLeft || isRight || isAbove || isBelow)            
  }   
  
}