package fp

import scala.collection.mutable

import scala.io.StdIn

import image.Image
import image.ImageUtility
import image.Position
import selection.Selection
import selection.SelectionPart
import scala.util.matching.Regex
import selection.NoSuchSelectionException
import layer.LayerContainer
import layer.Layer
import image.Pixel
import operation._
import operation.arithmetic._
import operation.predefined._
import operation.functional._
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer

class REPL {

  private var doRun: Boolean = true; 

  private val ldctx = raw"context load (.*?)".r
  private val svctx = raw"context save (.*?)".r
  
  private val ldimg = raw"image load (.*?)".r
  private val svimg = raw"image save (.*?)".r
  private val svlay = raw"image layered save (.*?)".r
  
  private val newselection = raw"selection new (.*) (\d+) (\d+) (\d+) (\d+)".r
  private val deleteselection = raw"selection delete (.*)".r
  private val activateselection = raw"selection activate (.*)".r
  private val deactivateselection = raw"selection deactivate (.*)".r
  private val addselectionpart = raw"selection add (.*) (\d+) (\d+) (\d+) (\d+)".r
  private val removeselectionpart = raw"selection remove (.*) (\d+)".r
  private val paintselection = raw"selection fill (.*) ([0-9]*[.]?[0-9]+) ([0-9]*[.]?[0-9]+) ([0-9]*[.]?[0-9]+)".r
  private val PrintSelection = "selection all"
  private val PrintLayers = "layer all"
  private val PrintOperations = "operation all"
  
  private val newlayer = raw"layer new (.*)".r  
  private val duplicatelayer = raw"layer duplicate (\d+)".r
  private val movelayer = raw"layer move (\d+) (\d+)".r
  private val movelayerup = raw"layer move up (\d+)".r
  private val movelayerdown = raw"layer move down (\d+)".r
  private val translatelayer = raw"layer translate (\d+) (\d+) (\d+)".r
  private val translatelayertopleft = raw"layer translate (\d+) top left".r
  private val translatelayertopright = raw"layer translate (\d+) top right".r
  private val translatelayerbottomleft = raw"layer translate (\d+) bottom left".r
  private val translatelayerbottomright = raw"layer translate (\d+) bottom right".r
  private val activatelayer = raw"layer activate (\d+)".r
  private val deactivatelayer = raw"layer deactivate (\d+)".r
  private val paintlayer = raw"layer fill (\d+) ([0-9]*[.]?[0-9]+) ([0-9]*[.]?[0-9]+) ([0-9]*[.]?[0-9]+)".r
  private val visiblelayer = raw"layer visible (\d+)".r
  private val invisiblelayer = raw"layer invisible (\d+)".r
  private val deletelayer = raw"layer delete (\d+)".r
  private val setopacity = raw"layer opacity (\d+) ([0-9]*[.]?[0-9]+)".r
  
  private val applyoperation = raw"operation apply (.+)".r
  private val defineoperation = raw"operation define (.+)".r
  private val composeoperation = raw"operation compose (.+)".r
  private val sequenceoperation = raw"operation sequence (.+)".r
     
  
  def test = {
    /*Context.load("penguin.jpg")
    Context.addLayer("otter.jpg")
    Context.addLayer("wombat.jpg")
    Context.setOpacity(1, 0.5)
    Context.moveLayerUp(1)
    Context.moveLayerDown(2)
    Context.newSelection("twoSquares", 200, 200, 400, 400)
    Context.addPart("twoSquares", 500, 500, 600, 600)
    val lst: List[Function] = List(AddOperation.toFunction(0.2), SubOperation.toFunction(0.2))
    Context.composeFunction("addsub add 0.2 sub 0.2")
    //val op = new SequentialOperation(lst, "add 0.2 sub 0.2", "addsub")                 
    Context.toMF("mf.txt")*/    
  }
  
  def run() {

    while (doRun) {
      try {
        print("fp: ");
        StdIn.readLine() match {

          
          /* OPERACIJE SA SLIKOM */
          case ldctx(ctxname) => Context.fromMF(ctxname)
          case svctx(ctxname) => Context.toMF(ctxname)          
          case ldimg(imgname) => Context.load(imgname)
          case "image show"   => Context.show
          case svimg(imgname) => Context.save(imgname)
          case svlay(imgname) => Context.saveLayered(imgname)
                               
          /* SELEKCIJE */
          case newselection(name, x1, y1, x2, y2) => 
            Context.newSelection(name, x1.toInt, x2.toInt, y1.toInt, y2.toInt)                              
          case deleteselection(name) => Context.deleteSelection(name)
          case activateselection(name) => Context.activateSelection(name)
          case deactivateselection(name) => Context.deactivateSelection(name)
          case paintselection(name, r, g, b) => Context.fillSelection(name, r.toDouble, g.toDouble, b.toDouble)
          case addselectionpart(name, x1, y1, x2, y2) 
            => Context.addPart(name, x1.toInt, y1.toInt, x2.toInt, y2.toInt)            
          case removeselectionpart(name, id) => Context.removePart(name, id.toInt)
          /* SELEKCIJE */
          
          
          /* SLOJEVI */
          case newlayer(imgname) => Context.addLayer(imgname)                      
          case duplicatelayer(id) => println(id)
          case movelayer(id, pos) => Context.moveLayer(id.toInt, pos.toInt)
          case movelayerup(id) => Context.moveLayerUp(id.toInt)
          case movelayerdown(id) => Context.moveLayerDown(id.toInt)
          case translatelayer(id, x, y) => Context.translateLayer(id.toInt, x.toInt, y.toInt)
          case translatelayertopleft(id) => Context.translateLayerTopLeft(id.toInt)
          case translatelayertopright(id) => Context.translateLayerTopRight(id.toInt)
          case translatelayerbottomleft(id) => Context.translateLayerBottomLeft(id.toInt)
          case translatelayerbottomright(id) => Context.translateLayerBottomRight(id.toInt)
          case activatelayer(id) => Context.activateLayer(id.toInt)
          case deactivatelayer(id) => Context.deactivateLayer(id.toInt)
          case paintlayer(id, r, g, b) => Context.fillLayer(id.toInt, r.toDouble, g.toDouble, b.toDouble)
          case visiblelayer(id) => Context.setVisibleLayer(id.toInt)
          case invisiblelayer(id) => Context.setInvisibleLayer(id.toInt)
          case deletelayer(id) => Context.deleteLayer(id.toInt)
          case setopacity(id, opacity) => Context.setOpacity(id.toInt, opacity.toDouble)
          /* SLOJEVI */
          
          /* OPERACIJE */                    
          case defineoperation(opdata) => Context.defineFunction(opdata)         
          case composeoperation(opdata) => Context.composeFunction(opdata)          
          case sequenceoperation(opdata) => Context.sequenceFunction(opdata)          
          case applyoperation(opdata) => Context.applyFunction(opdata)
          /* OPERACIJE */
          
          /* PRINTOUT */
          case PrintSelection  => Context.selectionInfo 
          case PrintLayers     => Context.layerInfo
          case PrintOperations => Context.functionInfo
          case "show all" => {
            Context.selectionInfo 
            Context.layerInfo
            Context.functionInfo            
          }
          
          case "exit" => doRun = false
          case _      => println("Bad query!")
        } 
      } catch {
        case e: Exception => println("Error: " + e);
      }
    }
  }
}