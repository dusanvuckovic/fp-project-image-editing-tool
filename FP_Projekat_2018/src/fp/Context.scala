package fp

import scala.collection.mutable

import image.Image
import image.ImageUtility
import image.Position
import selection.Selection
import selection.SelectionPart
import scala.util.matching.Regex
import selection.NoSuchSelectionException
import layer.LayerContainer
import layer.Layer
import image.Pixel
import operation._
import operation.arithmetic._
import operation.predefined._
import operation.functional._
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer
import java.io.File
import java.io.BufferedWriter
import java.io.OutputStreamWriter
import java.nio.charset.Charset
import java.io.PrintWriter
import scala.io.Source

object Context {

  private var image: Option[Image] = None
  private val selections: mutable.Map[String, Selection] = mutable.Map.empty[String, Selection]
  private val layers: LayerContainer = new LayerContainer
  
  private val originalOperations: mutable.Map[String, Function] = mutable.Map(
    ("abs" -> AbsFunction),
    ("inv" -> InversionFunction),
    ("log" -> LogFunction),
    ("gsc" -> GrayscaleFunction))
  
  private val argumentOperations: mutable.Map[String, ArithmeticOperation] =
    mutable.Map(
      ("add" -> AddOperation),
      ("sub" -> SubOperation),
      ("subi" -> SubInvOperation),
      ("mul" -> MulOperation),
      ("div" -> DivOperation),
      ("divi" -> DivInvOperation),
      ("pow" -> PowFunction),
      ("min" -> MinFunction),
      ("max" -> MaxFunction))

  private val operations: mutable.Map[String, Function] = mutable.Map(
    ("abs" -> AbsFunction),
    ("inv" -> InversionFunction),
    ("log" -> LogFunction),
    ("gsc" -> GrayscaleFunction))

  private val newlyDefined: mutable.Map[String, MultipartFunction] = mutable.Map.empty

  private def getPixels(image: Image): List[Vector[Pixel]] = {
    if (selections.isEmpty)
      List(image.pixels)
    else {
      selections.values.map(s =>
        image.pixels.filter(pixel => s.containsPixel(pixel))).toList
    }
  }
  
  private def applyFunctionToLayer(l: Layer, f: Function) = {
    val pixels = l.image.pixels.map(p => f(p))    
  }

  private def applyFunctionUnilaterally(function: Function): Unit = {
    val pixels = image.get.pixels
      .map(p => function(p))
    for (pixel <- pixels)
      image.get(pixel.position.x, pixel.position.y).color = pixel.color
    pixels.map(p => p.clamp)
  }

  def applyFunction(function: Function, s: Selection): Unit = {
    val pixels = image.get.pixels
      .filter(pixel => s.containsPixel(pixel))
      .map(p => function(p))
    for (pixel <- pixels)
      image.get(pixel.position.x, pixel.position.y).color = pixel.color
    pixels.map(p => p.clamp)
  }   

  def applyFunction(function: Function, image: Image): Unit = {
    val pixelCollection = getPixels(image)
    pixelCollection.foreach(
      pixels => pixels.map(p => function(p)))
    if (selections.isEmpty)
      applyFunctionUnilaterally(function)
    else
      selections.values.foreach(sel => applyFunction(function, sel))
  }
  
  def applyFunctionToAll(function: Function): Unit = {
    val buf: ArrayBuffer[Image] = ArrayBuffer(image.get)
    val images = layers.filter(_.active).map(layer => layer.image).toArray
    buf ++= images
    buf.map(image => applyFunction(function, image))
  }

  def runFunction(opdata: String): Unit = {
    val fn = getFunction(opdata)
    applyFunctionToAll(fn)
  }

  def toMF(fpath: String) = {    
    val file = new File(fpath)
    val writer = new PrintWriter(file)
    writer.write(image.getOrElse(throw new Exception("Slika nije definisana!")).toMF + "\n")
    var selectionStr = ""
    selections.values.foreach(sel => selectionStr += sel.toMF + "\n")
    writer.write(selectionStr)
    var layerStr = ""
    layers.foreach(lay => layerStr += lay.toMF + "\n")
    writer.write(layerStr)
    var opStr = ""
    newlyDefined.values.foreach(op => opStr += op.toMF + "\n")
    writer.write(opStr)
    writer.close()
  }

  def fromMF(fpath: String) = {

    selections.clear()
    layers.clear
    operations.clear()
    originalOperations map {case (k, v) => operations.put(k, v)}    
    
    val lines = Source.fromFile(fpath).getLines.filter(line => line.trim.nonEmpty && !line.startsWith(raw"//")).toArray
    for (line <- lines) {
      val name = line.split(" ")(0)
      val rest = line.split(" ").drop(1).mkString(" ")
      name match {
        case "image" => image = Some(ImageUtility.load(rest))
        case "selection" => {
          val selection = Selection.fromMF(rest)
          selections += (selection.name -> selection)
        }
        case "layer"     => layers += Layer.fromMF(rest, image.get)
        case "composite" => Context.composeFunction(rest)
        case "sequence"  => Context.sequenceFunction(rest)
      }
    }
  }

  /* IMAGE OPS */

  def load(imgname: String) = image = Some(ImageUtility.load(imgname))
  def save(imgname: String) = ImageUtility.save(image.get, imgname)
  def saveLayered(imgname: String) = ImageUtility.save(image.get, layers, imgname)
  def show = ImageUtility.show(image.get, layers)

  /* SELECTIONS */
  def newSelection(name: String, x1: Int, x2: Int, y1: Int, y2: Int) = {
    val p1 = Position(x1, y1)
    val p2 = Position(x2, y2)
    this.selections += (name -> new Selection(name, p1, p2))
  }

  def deleteSelection(name: String) = {
    if (!selections.contains(name))
      throw new NoSuchSelectionException(name)
    selections.remove(name)
  }

  def activateSelection(name: String) = {
    if (!selections.contains(name))
      throw new NoSuchSelectionException(name)
    selections(name).setActive
  }

  def deactivateSelection(name: String) = {
    if (!selections.contains(name))
      throw new NoSuchSelectionException(name)
    selections(name).setInactive
  }

  def addPart(name: String, x1: Int, x2: Int, y1: Int, y2: Int) = {
    if (!selections.contains(name))
      throw new NoSuchSelectionException(name)
    selections(name) += (Position(x1, y1), Position(x2, y2))
  }

  def removePart(name: String, id: Int) = {
    if (!selections.contains(name))
      throw new NoSuchSelectionException(name)
    selections(name) -= id
  }
  
  def fillSelection(name: String, r: Double, g: Double, b: Double) = {
    if (!selections.contains(name))
      throw new NoSuchSelectionException(name)
    val sel = selections(name)
    val op = new FillOperation(r, g, b)
    applyFunction(op, sel)    
  }

  def selectionInfo: Unit = {
    if (selections.isEmpty) println("Selekcije: \n\tnema!") else selections foreach (p => println(p._2))  
  }

  /* SELECTIONS */

  /* LAYERS */

  def addLayer(imageName: String) = {
    val layerImage = ImageUtility.load(imageName)
    layers += new Layer(layerImage, this.image.get)
  }
  def moveLayer(id: Int, pos: Int) = layers.move(id, pos)
  def moveLayerUp(id: Int) = layers.moveUp(id)
  def moveLayerDown(id: Int) = layers.moveDown(id)
  def translateLayer(id: Int, x: Int, y: Int) = layers.translateTo(id, Position(x, y))
  def translateLayerTopLeft(id: Int) = layers.translateToTopLeft(id)
  def translateLayerTopRight(id: Int) = layers.translateToTopLeft(id)
  def translateLayerBottomLeft(id: Int) = layers.translateToTopLeft(id)
  def translateLayerBottomRight(id: Int) = layers.translateToTopLeft(id)
  def setOpacity(id: Int, opacity: Double) = layers(id).opacity = opacity
  def activateLayer(id: Int) = layers(id).setActive
  def deactivateLayer(id: Int) = layers(id).setInactive
  def setVisibleLayer(id: Int) = layers(id).setVisible
  def setInvisibleLayer(id: Int) = layers(id).setInvisible
  def deleteLayer(id: Int) = layers -= id
  def fillLayer(id: Int, r: Double, g: Double, b: Double) = {
    val op = new FillOperation(r, g, b)
    applyFunctionToLayer(layers(id), op)    
  }
  def layerInfo = println(layers)

  /* LAYERS */

  /* FUNCTIONS */

  private def numOfParameters(name: String) = {
    if (name == "wgh")
      2
    else if (name == "med" || argumentOperations.contains(name))
      1
    else
      0
  }

  private def dropCurrentOperation(opdata: String): String = {
    val data = opdata.split(" ")
    val opname = data(0)

    numOfParameters(opname) match {
      case 0 => {
        data.drop(1).mkString(" ")
      }
      case 1 => {
        data.drop(2).mkString(" ")
      }
      case 2 => {
        val n = data(1).toInt
        val parsrow = 1 + 2 * n
        data.drop(2 + parsrow * parsrow).mkString(" ")
      }
      case _ => throw new Exception("Loše unet format složene funkcije!")
    }
  }

  private def getFunction(opdata: String): Function = {
    val data = opdata.split(" ")
    val opname = data(0)

    numOfParameters(opname) match {
      case 0 => {
        operations(opname)
      }
      case 1 => {
        if (opname == "med") {
          new MedianFilterFunction(data(1).toInt, image.get)
        } else {
          val fn = argumentOperations(opname)
          val oparg = data(1).toDouble
          fn.toFunction(oparg)
        }
      }
      case 2 => {
        val n = data(1).toInt
        var matrix = data.drop(2).map(str => str.toDouble)
        opname match {
          case "wgh" => {
            val parsrow = 1 + 2 * n
            if (matrix.size != parsrow * parsrow)
              throw new Exception(s"Loš broj parametara za matricu ${n + 1}x${n + 1}: " +
                s"${matrix.size} a treba ${parsrow * parsrow}!")
            val buff = ArrayBuffer[Array[Double]]()
            while (matrix.nonEmpty) {
              val tempAr = matrix.take(parsrow)
              buff += tempAr
              matrix = matrix.drop(n)
            }
            new WeightedMeanFilterFunction(n, buff.toArray, image.get)
          }
          case _ => throw new Exception("Loše ime operacije!")
        }
      }
    }
  }

  private def getFunctions(opdata: String): List[Function] = {

    var opString: String = opdata
    var words = opdata.split(" ")
    opString = words.drop(1).mkString(" ")
    val lst = ListBuffer[Function]()
    while (opString.trim() != "") {
      lst += getFunction(opString)
      opString = dropCurrentOperation(opString)
    }
    lst.toList
  }

  def defineFunction(opdata: String) = {
    val words = opdata.split(" ")
    val name = words(0)
    val fn = getFunction(words.drop(1).mkString(" "))
    operations.put(name, fn)
  }

  def composeFunction(opdata: String) = {
    val fns = getFunctions(opdata)
    val name = opdata.split(" ")(0)
    val compop = new CompositeFunction(fns, opdata)
    operations.put(name, compop)
    newlyDefined.put(name, compop)
  }

  def sequenceFunction(opdata: String) = {
    val fns = getFunctions(opdata)
    val name = opdata.split(" ")(0)
    val seqop = new SequentialOperation(fns, opdata)
    operations.put(name, seqop)
    newlyDefined.put(name, seqop)
  }

  def applyFunction(opdata: String) = {
    runFunction(opdata)
  }

  def functionInfo: Unit = {
    val str1 = argumentOperations.keys.map(name => s"\t$name p").mkString("\n")
    val str2 = operations.keys.map(name => s"\t$name").mkString("\n")
    println(s"Operacije:\n$str2\n\n$str1\n\n\tmed p\n\n\twgh p NxN")
  }
  /* FUNCTIONS */

}