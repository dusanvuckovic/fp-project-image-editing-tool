package image

import javax.swing.JFrame
import javax.swing.ImageIcon
import javax.swing.JLabel
import java.awt.Toolkit

class ImageView(image: Image, opacity: Double) {
  
  val frame: JFrame = new JFrame(image.fpath)
  frame.setSize(image.width, image.height)      
  frame.getContentPane().add(new JLabel(new ImageIcon(image.toJavaImage(opacity))))
  frame.setResizable(false)
  
  val screenWidth = Toolkit.getDefaultToolkit().getScreenSize().getWidth;
  val screenHeight = Toolkit.getDefaultToolkit().getScreenSize().getHeight;
  
  val x = ((screenWidth - frame.getSize().width)/2).toInt;
  val y = ((screenHeight - frame.getSize().height)/2).toInt;

  frame.setLocation(x, y);
      
  def show = frame.setVisible(true)
  
  def this(image: Image) = 
    this(image, 1d)   
  
}