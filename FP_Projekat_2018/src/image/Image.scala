package image

import java.awt.image.BufferedImage

import selection.Selection
import layer.Layer
import selection.Area
import javax.imageio.ImageIO

object Image {
  def fromMF(str: String): Image = {    
    val fn = str.substring(str.indexOf("image "))
    ImageUtility.load(fn)   
  }
}

class Image(width: Int, height: Int, val pixels: Vector[Pixel], 
    val fpath: String) extends Area(width, height) {
  if (width < 0)
    throw new IllegalArgumentException("width should be < 0")
  if (height < 0)
    throw new IllegalArgumentException("height should be < 0")   
   
  def apply(i: Int, j: Int): Pixel = {
    pixels(i*height + j)
  }    
  
  override def toString = s"width = $width, height = $height"
  
  def show = ImageUtility.show(this)
  
  def applyLayer(layer: Layer): Image = {
    var g = 0
    for {
      i <- layer.rangeX
      j <- layer.rangeY      
    } this(i, j).color.lerp(layer(i, j).color, layer.opacity)    
    this
  }
  
  def toJavaImage: BufferedImage = toJavaImage(1d)  
  
  def toJavaImage(opacity: Double): BufferedImage = {    
    val javaImage = new BufferedImage(this.width, this.height, BufferedImage.TYPE_3BYTE_BGR)
    for (pixel <- this.pixels) {      
      val r = (pixel.color.r * 255).toInt
      val g = (pixel.color.g * 255).toInt
      val b = (pixel.color.b * 255).toInt
      val a = (opacity * 255).toInt
           
      val rgb: Int = b | g << 8 | r << 16 | a << 24
      javaImage.setRGB(pixel.position.x, pixel.position.y, rgb)      
    }
    javaImage       
  }
  
  def toMF =
    s"image ${fpath}"    
  
                   

}