package image

import javax.imageio.ImageIO
import java.io.File
import java.awt.image.BufferedImage 
import java.nio.file.Files
import java.nio.file.Paths 
import java.nio.file.StandardOpenOption 
import layer.LayerContainer

object ImageUtility {
  
  val ldLocation = "src/data/"
  
  def load(filename: String): Image = {
    val img = ImageIO.read(new File(ldLocation+filename))
    convertToImage(img, filename)    
  }
  
  def show(image: Image): Unit =
    show(image, 1d)
  
  def show(image: Image, opacity: Double) = {
    val view = new ImageView(image, opacity)
    view.show    
  }
  
  def layeredImage(image: Image, layers: LayerContainer): Image = {
    var endImage = new Image(image.width, image.height, image.pixels.map(_.copy), image.fpath)
    for (layer <- layers.filter(_.visible))
      endImage = endImage applyLayer layer
    endImage
  }
  
  def show(image: Image, layers: LayerContainer) = {    
    val endImage = layeredImage(image, layers)
    val view = new ImageView(endImage)
    view.show
  }
  
  def save(image: Image, fpath: String) = {        
    ImageIO.write(image.toJavaImage(1), fpath.substring(fpath.indexOf(".")+1), new File(ldLocation+fpath))    
  }
  
  def save(image: Image, layers: LayerContainer, fpath: String) = { 
    val lImg = layeredImage(image, layers)
    ImageIO.write(lImg.toJavaImage(1), fpath.substring(fpath.indexOf(".")+1), new File(ldLocation+fpath))    
  }
  
  def convertToImage(image: BufferedImage, filename: String): Image = {
    val width = image.getWidth
    val height = image.getHeight       
      
    val data: Vector[Pixel] = Vector.tabulate(width*height) (    
        (pos) => {
          val i = pos / height
          val j = pos % height
          val rgb = image.getRGB(i, j) /*uvek u sRGB koji god format slike bio*/  
          //val a = ((rgb >> 24) & 0xFF) / 255d
          val r = ((rgb >> 16) & 0xFF) / 255d
          val g = ((rgb >> 8) & 0xFF) / 255d
          val b = (rgb & 0xFF) / 255d          
          val color = new Color(r, g, b)
          val position = new Position(i, j)
          new Pixel(color, position)
        }
    );
    new Image(width, height, data, filename)              
  }   
  
  
}