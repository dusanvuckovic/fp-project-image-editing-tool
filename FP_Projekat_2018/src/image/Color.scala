package image


class Color(var r: Double, var g: Double, var b: Double/*, var a: Double*/) {  
  
  
  def check = {
    if (r < 0) 
      throw new IllegalArgumentException("r should be > 0");
    if (g < 0)
      throw new IllegalArgumentException("g should be > 0");
    if (b < 0)
      throw new IllegalArgumentException("b should be > 0");
    if (r > 1d) 
      throw new IllegalArgumentException("r should be < 1");
    if (g > 1d) 
      throw new IllegalArgumentException("g should be < 1");
    if (b > 1d) 
      throw new IllegalArgumentException("b should be < 1");    
  }
    
  def +(that: Color) =
    new Color(this.r + that.r, this.g + that.g, this.b + that.b)
  
  def +(that: Double) =
    new Color(this.r + that, this.g + that, this.b + that)
    
  def *(that: Double): Color = {
    new Color(this.r*that, this.g*that, this.b*that)    
  }
  
  def /(that: Double): Color = {
    new Color(this.r/that, this.g/that, this.b/that)    
  }
  
  def copy: Color = 
    new Color(r, g, b)   
  
  def lerp(newColor: Color, op: Double): Unit = { 
    this.r = this.r * (1-op) + newColor.r * op
    this.g = this.g * (1-op) + newColor.g * op
    this.b = this.b * (1-op) + newColor.b * op
  }
  
  override def toString = s"(r: $r, g: $g, b:$b)"      
  
} 