package image

case class Position(x: Int, y: Int) {
  
  override def toString = s"($x, $y)"
  
  def + (position: Position): Position =
    Position(x + position.x, y + position.y)
    
  def + (x: Int, y: Int): Position = 
    this + Position(x, y)
  
  def - (position: Position): Position =
    Position(x - position.x, y - position.y)
    
  def - (x: Int, y: Int): Position = 
    this - Position(x, y)
    
}