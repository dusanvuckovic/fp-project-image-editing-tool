package image
import scala.language.implicitConversions

class Pixel(var color: Color, val position: Position) extends Ordered[Pixel] {
  
  override def toString =
    s"$color on $position"
    
  def copy: Pixel =
    new Pixel(color.copy, position)
  
  def apply(f: Double => Double): Pixel = {
    color.r = f(color.r)
    color.g = f(color.g)
    color.b = f(color.b)
    this
  }
  
  def clamp = {
    color.r = if (color.r > 1) 1 else if (color.r < 0) 0 else color.r
    color.g = if (color.r > 1) 1 else if (color.r < 0) 0 else color.g
    color.b = if (color.r > 1) 1 else if (color.r < 0) 0 else color.b         
  }
  
  private def rgb2xyz = {
    var r = color.r
    var g = color.g
    var b = color.b
  
    r = if (r > 0.04045d) Math.pow((r + 0.055d)/1.055d, 2.4d) else r / 12.92d
    g = if (g > 0.04045d) Math.pow((g + 0.055d)/1.055d, 2.4d) else g / 12.92d    
    b = if (b > 0.04045d) Math.pow((b + 0.055d)/1.055d, 2.4d) else b / 12.92d
    
    r *= 100
    g *= 100
    b *= 100
    
    val X = r * 0.4124 + g * 0.3576 + b * 0.1805
    val Y = r * 0.2126 + g * 0.7152 + b * 0.0722
    val Z = r * 0.0193 + g * 0.1192 + b * 0.9505
    
    (X, Y, Z)
  }
  
  implicit def rgb2lab(p: Pixel) = {
    val xyz = p.rgb2xyz
    
    var x = xyz._1 / 95.047
    var y = xyz._2 / 100.0
    var z = xyz._3 / 108.883
    
    x = if (x > 0.008856) Math.pow(x, 1/3) else (7.787 * x) + (16 / 116)
    y = if (y > 0.008856) Math.pow(y, 1/3) else (7.787 * y) + (16 / 116)
    z = if (z > 0.008856) Math.pow(z, 1/3) else (7.787 * z) + (16 / 116)
    
    val l = (116 * y) - 16
    val a = 500 * (x - y)
    val b = 200 * (y - z)    
        
    new ColorLab(l, a, b)
  }
   
  def compare (that: Pixel) = {    
    val c1: ColorLab = this
    val c2: ColorLab = that
    
    val deltaeab = Math.sqrt(
      (c2.l - c1.l)*(c2.l - c1.l) + 
      (c2.a - c1.a)*(c2.a - c1.a) + 
      (c2.b - c1.b)*(c2.b - c1.b) 
    )
    if (deltaeab < 0.01) 0 else if (deltaeab > 0) 1 else -1              
  }
  
}